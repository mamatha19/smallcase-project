import React from "react";
import "./App.css";
import Header from "./components/header";
import { ThemeProvider, CSSReset } from "@chakra-ui/core";

function App() {
  return (
    <ThemeProvider>
      <CSSReset />
      <div class="App">
        <Header />
      </div>
    </ThemeProvider>
  );
}

export default App;
